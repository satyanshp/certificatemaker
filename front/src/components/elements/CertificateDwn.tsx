import React, { useEffect } from 'react'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

interface CerProps{
    CertiData:{
        name:string,
        duration:string,
        position:string,
        startDate:string,
        endDate:string,
      },
}
const CertificateDwn = ({CertiData}:CerProps) => {
    useEffect(() => {
        console.log(CertiData)
      }, [CertiData])
  return (
    <Box 
        minHeight='100vh' 
        // sx={{aspectRatio:'16/9'}}
    >
        <Grid height='100vh' container direction={{xs:'row'}}>
            <Grid item sm={3} xs={12} sx={{background:'#1D1263'}} height={{sm:'100%',xs:'30%'}} display='flex' flexDirection={{sm:'column'}} justifyContent='space-between' padding={{sm:'80px 0 50px',xs:'30px'}}>
                <Box display='flex' flexDirection='column' gap={1.5} justifyContent='center' marginLeft={{sm:'0',xs:'10px'}}>
                    <Box  display='flex' width='100%' height={{sm:'80px',xs:'50px'}}><img style={{margin:'0 auto',height:'100%'}} src="/assets/images/logo.svg" alt="" /></Box>
                    <Box
                            fontSize= {{sm:'35px',xs:'25px'}}
                            lineHeight= '47px'
                        sx={{
                            fontFamily: 'Roboto',
                            fontStyle: 'normal',
                            fontWeight: '500',
                            textAlign: 'center',
                            color: '#E6E6FD',             
                        }}
                        className='bold'
                    >SimpleM</Box>
                </Box>
                <Box 
                        fontSize= {{sm:'16px',xs:'10px'}}
                        fontWeight='400'
                        lineHeight= {{sm:'22px',xs:'16px'}}
                    sx={{
                        fontFamily: 'Roboto',
                        fontStyle: 'normal',
                        color: '#F9F9FF'       
                    }}
                    padding={{sm:'0 40px',xs:'0px'}}
                    display='flex'
                >
                    <Box margin='auto 0'>
                        <Box 
                            className='extra_bold'>Contact Us :
                        </Box>
                        <Box height={{sm:25,xs:15}}/>
                        <Box display='flex' gap={{sm:3,xs:1.5}}>
                            <Box><img className='img_lg' src="/assets/images/Vector_2.svg" alt="" /></Box>
                            <Box>Plot number 15, Block T, KP2,<br/> Jaypee Wishtown,<br/> Sector 131, Noida, UP 201304</Box>
                        </Box>
                        <Box height={{sm:15,xs:8}}/>
                        <Box display='flex' gap={{sm:3,xs:1.5}}>
                            <Box><img className='img_lg' src="/assets/images/Vector_0.svg" alt="" /></Box>
                            <Box>contact@simplem.in</Box>
                        </Box>
                        <Box height={{sm:15,xs:8}}/>
                        <Box display='flex' gap={{sm:3,xs:1.5}}>
                            <Box><img className='img_lg' src="/assets/images/Vector_1.svg" alt="" /></Box>
                            <Box>www.simplem.in</Box>
                        </Box>
                    </Box>
                </Box>
            </Grid>
            <Grid item height={{sm:'100%',xs:'70%'}} sm={9} xs={12} className='content_section' padding={{sm:'0 70px',xs:'0 35px'}} position='relative' sx={{'@media (max-width: 600px)':{backgroundSize:'cover'}}} display='flex' flexDirection='column' justifyContent='space-evenly'>
                <Box position='absolute' top='0' left={{sm:'90%',xs:'85%'}} className='frame'><img src="/assets/images/Frame.svg" alt="" /></Box>
                <Box>
                    <Box 
                        fontSize={{sm:'50px',xs:'25px'}}
                        lineHeight={{sm:'60px',xs:'32px'}}
                        sx={{
                            fontFamily: 'Roboto',
                            fontStyle: 'normal',
                            color: '#1D1263',  
                            fontWeight: '500',        
                        }}
                        className='bold'
                    >
                        CERCERTIFICATE OF <br/> 
                        <Box component='span'
                            fontSize= {{sm:'60px',xs:'32px'}}
                            lineHeight= {{sm:'70px',xs:'42px'}}
                        >
                            COMPLETION
                        </Box> 
                    </Box>
                </Box>
                <Box
                    fontSize= {{sm:'25px',xs:'16px'}}
                    lineHeight= {{sm:'30px',xs:'20px'}}
                    sx={{
                        fontFamily: 'Roboto',
                        fontStyle: 'normal',
                        fontWeight: '300',
                        color: '#000000',        
                    }}
                    width='90%'
                    className='content'
                >
                    <Box component='span'>
                        This is to ceritify that <br/>
                    </Box>
                    <Box height={10}/>
                    <Box component='span' fontFamily='Allura' fontSize={{sm:'55px',xs:'30px'}} lineHeight={{sm:'77.5px',xs:'40px'}} position='relative' className='name'>{CertiData.name? CertiData.name : 'Avishi Goyal'} <br/></Box>
                    <Box height={10}/>
                    <Box component='span' lineHeight= '30px'>
                        has successfully completed her <span style={{fontWeight:'500'}} className='bold'>{CertiData.duration? CertiData.duration :'2'} months</span> internship in <span style={{fontWeight:'500'}} className='bold'>Simplem Solutions Private Limited</span> in field of <span style={{fontWeight:'500'}} className='bold'>{CertiData.position? CertiData.position :'UI/UX Design'}</span>.<br/>
                    </Box>
                    <Box height={30}/>
                    <Box component='span'>Duration : <span style={{fontWeight:'500'}} className='bold'>{CertiData.startDate? CertiData.startDate :'20 July 2022'} to {CertiData.endDate? CertiData.endDate :'20 Sep 2022'}</span></Box>
                </Box>
                <Box display='flex' flexDirection='column' alignItems='flex-end'>
                    <Box minWidth={{sm:'330px',xs:'230px'}}
                            fontSize= {{sm:'25px',xs:'18px'}}
                            lineHeight= {{sm:'35px',xs:'28px'}}
                        sx={{
                            fontFamily: 'Roboto',
                            fontStyle: 'normal',
                            fontWeight: '300',
                            color: '#000000',        
                        }}
                    >
                        <Box textAlign='center' fontWeight='500' className='bold'>Prashant Choudhary</Box>
                        <Box height='0.8px' width='100%' sx={{background:'#000000'}}/>
                        <Box textAlign='center'>CHIEF TECHNICAL OFFICER</Box>
                    </Box>
                </Box>
            </Grid>
        </Grid>
    </Box>
  )
}

export default CertificateDwn